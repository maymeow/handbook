---
title: Level Up
---

## Leveling Up Level Up

_TEASER_: Get ready for **HUGE** changes coming to learning technology for GitLab team members! We are upgrading the technology solution that powers Level Up to better support the pace, demand, complexity, and CULTURE of GitLab. This overhaul will allow team members to engage around learning in a COMPLETELY different way. Learning today goes way beyond self-paced content, far passed webinars, leaps above documents; learning is much much more. And GitLab's new "**LevelUp**" (integrated talent EXPERIENCE platform) will show you just how dynamic learning, growth, skills development, and performance can go. Consider this page your LevelUp information highway. Over the coming weeks, you'll see updates, announcements, and rollout plans here.

{{< gdoc >}}
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSUJWcpfq-Y_4C7qOIMAT11wJX0n5pAfuKUp9xnQZvuyrESb87_ZISah83q42b4bo-HHOxMnHvNRJSH/embed?start=false&loop=false&delayms=60000"
frameborder="0" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
{{</ gdoc >}}
